/*
 * Copyright 2019 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.plex.parse;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import org.junit.Before;

/**
 *
 * @author Roberto C. Benitez
 */
public class BasePlexTest
{

    protected String plexServer;
    protected String authToken;

    public BasePlexTest()
    {
    }

    @Before
    public void init()
    {
        Path path = Paths.get(System.getProperty("user.home"), ".PlexClient", "config.properties");
        Properties props = new Properties();
        try (final InputStream is = Files.newInputStream(path))
        {
            props.load(is);
            plexServer = props.getProperty("plexServer");
            authToken = props.getProperty("plexAuthToken");
        }
        catch (IOException e)
        {
            throw new RuntimeException("Unable to load Plex Client Configuration. " + e.getMessage(), e);
        }
    }

}
