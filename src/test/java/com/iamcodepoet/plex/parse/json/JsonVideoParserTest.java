/*
 * Copyright 2019 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.plex.parse.json;

import com.iamcodepoet.plex.Video;
import com.iamcodepoet.plex.VideoPart;
import com.iamcodepoet.plex.parse.BasePlexTest;
import java.io.IOException;
import java.util.List;
import org.junit.Test;

/**
 *
 * @author Roberto C. Benitez
 */
public class JsonVideoParserTest extends BasePlexTest
{

    public JsonVideoParserTest()
    {
    }

    @Test
    public void testSomeMethod() throws IOException
    {

        String path = String.format("library/metadata/16749/children?X-Plex-Token=%s", authToken);

        JsonVideoParser parser = new JsonVideoParser(plexServer);
        List<Video> videos = parser.parse(path);

        for (Video video : videos)
        {
            System.out.println(video);
            System.out.printf("\tVideos: %s%n", video.getParts().size());
            for (VideoPart part : video.getParts())
            {
                System.out.printf("\t %s%n", part.getKey(), part.getDuration());
            }
        }

    }

}
