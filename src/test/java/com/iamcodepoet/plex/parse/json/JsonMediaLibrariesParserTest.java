/*
 * Copyright 2019 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.plex.parse.json;

import com.iamcodepoet.plex.parse.BasePlexTest;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.junit.Test;

/**
 *
 * @author Roberto C. Benitez
 */
public class JsonMediaLibrariesParserTest extends BasePlexTest
{

    public JsonMediaLibrariesParserTest()
    {
    }

    @Test
    public void testSomeMethod() throws UnsupportedEncodingException, IOException
    {
        JsonMediaLibrariesParser parser = new JsonMediaLibrariesParser(plexServer);

        String uri = String.format("library/sections?X-Plex-Token=%s", authToken);
        parser.parse(uri).forEach(System.out::println);
    }

}
