/*
 * Copyright 2019 roberto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.plex.parse;

import com.iamcodepoet.plex.parse.json.JsonMediaDirectoryParser;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author roberto
 */
public class JsonMediaDirectoryParserTest extends BasePlexTest
{


    public JsonMediaDirectoryParserTest()
    {
    }


    @Test
    public void testSomeMethod() throws UnsupportedEncodingException, IOException
    {
        JsonMediaDirectoryParser parser = new JsonMediaDirectoryParser(plexServer);
        int library = 1;
        String search = URLEncoder.encode("rings", "utf-8");
        String uri = String.format("library/sections/%d/allLeaves?X-Plex-Token=%s&title=%s", library, authToken, search);
        parser.parse(uri).forEach(System.out::println);
    }

}
