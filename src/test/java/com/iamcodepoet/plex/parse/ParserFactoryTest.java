/*
 * Copyright 2019 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.plex.parse;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Roberto C. Benitez
 */
public class ParserFactoryTest
{

    public ParserFactoryTest()
    {
    }

    @Test(expected = ParserFactoryException.class)
    public void testFailure() throws ParserFactoryException
    {
        ParserFactory factory = ParserFactory.createFactory("The Imposible Factory");
    }

    @Test
    public void testJson()
    {
        try
        {
            final String json = "JSON";
            ParserFactory factory = ParserFactory.createFactory(json);
            Assert.assertEquals("Invalid Factory type.", factory.getType().toUpperCase(), json);
        }
        catch (ParserFactoryException e)
        {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testXml()
    {
        try
        {
            final String xml = "XML";
            ParserFactory factory = ParserFactory.createFactory(xml);
            Assert.assertEquals("Invalid Factory type.", factory.getType().toUpperCase(), xml);
        }
        catch (ParserFactoryException e)
        {
            Assert.fail(e.getMessage());
        }
    }

}
