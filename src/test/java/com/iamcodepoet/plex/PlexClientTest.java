/*
 * Copyright 2019 roberto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.plex;

import com.iamcodepoet.plex.parse.BasePlexTest;
import com.iamcodepoet.plex.parse.ParserFactoryException;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author roberto
 */
public class PlexClientTest extends BasePlexTest
{

    public PlexClientTest()
    {
    }

    @Test
    public void test()
    {
        //86,400,000
        long factor = 1000;
        System.out.println(new Date().getTime());
        long nowMillis = System.currentTimeMillis();
        long addedMillis = 1545790574 * factor;
        long updateMillis = 1581896830 * factor;

        System.out.printf("Now:\t%,d%n", nowMillis);
        System.out.printf("Added:\t%,d\t%n", addedMillis);
        System.out.printf("Update:\t%,d%n", updateMillis);

        System.out.println(new Date(addedMillis));
        System.out.println(new Date(updateMillis));

        System.out.println("");

    }

    @Test
    public void testListLibraries()
    {
        PlexClient client = new PlexClient(plexServer, authToken);
        try
        {
            List<MediaDirectory> libraries = client.getLibraries();
            libraries.forEach(System.out::println);
        }
        catch (IOException ex)
        {
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void testListTvShows()
    {
        PlexClient client = new PlexClient(plexServer, authToken);
        try
        {
            MediaDirectory tvShows = client.getLibraries()
                    .stream().filter(e -> e.getType().equals(DirectoryType.TV_SERIES.getPlexTypeName()))
                    .findFirst().get();

            List<MediaDirectory> list = client.getTvShowListingByLibraryKey(tvShows.getKey());
            list.forEach(System.out::println);
        }
        catch (IOException ex)
        {
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void testListTvShowSeasons()
    {
        PlexClient client = new PlexClient(plexServer, authToken);
        try
        {
            MediaDirectory tvShows = client.getLibraries()
                    .stream().filter(e -> e.getType().equals(DirectoryType.TV_SERIES.getPlexTypeName()))
                    .findFirst().get();

            List<MediaDirectory> list = client.getTvShowSeasonsByName(tvShows, "The Outer Limits");
            list.forEach(System.out::println);
        }
        catch (IOException ex)
        {
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void testListEpisodesBySeason()
    {
        PlexClient client = new PlexClient(plexServer, authToken);
        try
        {
            MediaDirectory tvShows = client.getLibraries()
                    .stream().filter(e -> e.getType().equals(DirectoryType.TV_SERIES.getPlexTypeName()))
                    .findFirst().get();

            List<MediaDirectory> list = client.getTvShowSeasonsByName(tvShows, "The Outer Limits");
            for (MediaDirectory season : list)
            {
                System.out.println(season.getTitle());
                List<Video> seasonShows = client.getEpisodesBySeason(season);
                seasonShows.forEach(e -> System.out.printf("\t%s%n", e.getTitle()));
            }
        }
        catch (IOException ex)
        {
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void testSearchTvShowInLibrary()
    {
        PlexClient client = new PlexClient(plexServer, authToken);
        try
        {
            MediaDirectory tvShows = client.getLibraries()
                    .stream().filter(e -> e.getType().equals(DirectoryType.TV_SERIES.getPlexTypeName()))
                    .findFirst().get();

            List<MediaDirectory> list = client.searchTvShow(tvShows, "Outer Limits");
            list.forEach(e -> System.out.printf("%s(%s)%n", e.getTitle(), e.getAttribute("originallyAvailableAt")));
        }
        catch (IOException ex)
        {
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void testSearchTvShows()
    {
        PlexClient client = new PlexClient(plexServer, authToken);
        try
        {
            List<MediaDirectory> list = client.searchTvShow("Outer Limits");
            list.forEach(e -> System.out.printf("%s(%s)%n", e.getTitle(), e.getAttribute("originallyAvailableAt")));
        }
        catch (IOException ex)
        {
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void testcurrentlyPlaying()
    {
        try
        {
            PlexClient client = new PlexClient(plexServer, authToken, "JSON");
            List<Video> playing = client.getVideoCurrentlyPlaying();
            playing.forEach(System.out::println);
        }
        catch (ParserFactoryException | IOException ex)
        {
            Throwable cause = ex.getCause() == null ? ex : ex.getCause();
            Assert.fail(cause.getMessage());
        }
    }

}
