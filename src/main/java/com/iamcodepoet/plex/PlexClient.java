/* 
 * Copyright 2019 roberto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.plex;

import com.iamcodepoet.plex.parse.Parser;
import com.iamcodepoet.plex.parse.ParserFactory;
import com.iamcodepoet.plex.parse.ParserFactoryException;
import com.iamcodepoet.plex.parse.xml.XmlParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * A client to get media information from a Plex server.
 *
 * @author roberto
 */
public class PlexClient
{

    public static final String PLEX_TOKEN_VAR = "X-Plex-Token";
    public static final String TV_SHOW_KEY_ATTR = "ratingKey";
    public static final String LIBRARIES_PATH = "/library/sections";
    public static final String LIBRARY_ENTRIES_PATH_TEMPLATE = "/library/sections/%s/all";
    public static final String ALL_SERIES_EPISODES_PATH_TEMPLATE = "/library/metadata/%s/allLeaves";
    public static final String CHILDREN_PATH_TEMPLATE = "library/metadata/%s/children";

    private final String server;
    private final String authenticationToken;
    private final ParserFactory parserFactory;

    public PlexClient(String server, String authentictionToken)
    {
        this(server, authentictionToken, new XmlParserFactory());
    }

    public PlexClient(String server, String authenticationToken, String parserFactoryType) throws ParserFactoryException
    {
        this(server, authenticationToken, ParserFactory.createFactory(parserFactoryType));
    }

    public PlexClient(String server, String authenticationToken, ParserFactory parserFactory)
    {
        this.server = Objects.requireNonNull(server, "Server cannot bve null");
        this.authenticationToken = Objects.requireNonNull(authenticationToken, "Authentication Token must not be null");
        this.parserFactory = Objects.requireNonNull(parserFactory, "Parser factory must not be null");
        this.parserFactory.setPlexServer(server);
    }

    /**
     * Get a list of all libraries.
     *
     * @return List of libraries
     * @throws IOException thrown if errors occur while reading plex data
     */
    public List<MediaDirectory> getLibraries() throws IOException
    {
        Parser<MediaDirectory> parser = parserFactory.createMediaLibraryParser();

        String path = LIBRARIES_PATH + createAuthTokenPart();
        return parser.parse(path);
    }

    /**
     * Get all the movies from the given movie library name
     *
     * @param libraryName Library Name
     * @return List of movies
     * @throws IOException thrown if errors occur while reading Plex data or the
     * named library does not exist
     */
    public List<Video> getMovieLibraryEntriesByName(String libraryName) throws IOException
    {
        Optional<MediaDirectory> opt = getLibraries().stream()
                .filter(e -> DirectoryType.MOVIE.isType(e) && e.getTitle().equals(libraryName))
                .findFirst();

        if (opt.isPresent())
        {
            return getMovieLibraryEntriesByKey(opt.get().getKey());
        }

        String msg = String.format("Movie library '%s' not found.", libraryName);
        throw new IOException(msg);
    }

    /**
     * Get all movies from the movie library with the given key.
     *
     * @param key Movie Library Key
     * @return List of movies
     * @throws IOException thrown if errors occur while reading Plex data
     */
    public List<Video> getMovieLibraryEntriesByKey(String key) throws IOException
    {
        Parser<Video> parser = parserFactory.createVideoParser();
        String path = createLibraryEntriesPath(key);

        return parser.parse(path);
    }

    public String createLibraryEntriesPath(String key)
    {
        return String.format(LIBRARY_ENTRIES_PATH_TEMPLATE, key) + createAuthTokenPart();
    }

    private String createAuthTokenPart()
    {
        String auth = (authenticationToken == null || authenticationToken.isEmpty()) ? "" : String.format("?%s=%s", PLEX_TOKEN_VAR, authenticationToken);
        return auth;
    }

    public List<MediaDirectory> searchTvShow(String showName) throws IOException
    {
        List<MediaDirectory> libraries = getLibraries().stream()
                .filter(e -> DirectoryType.TV_SERIES.isType(e)).collect(Collectors.toList());

        List<MediaDirectory> allResults = new ArrayList<>();
        for (MediaDirectory lib : libraries)
        {
            allResults.addAll(searchTvShow(lib, showName));
        }

        return allResults;
    }

    /**
     * Search TV Shows by name in given library.
     *
     * @param library target media library
     * @param showName target show name
     * @return list of shows
     * @throws IOException thrown if errors occur while downloading/parsing Plex
     * data
     */
    public List<MediaDirectory> searchTvShow(MediaDirectory library, String showName) throws IOException
    {
        if (!DirectoryType.TV_SERIES.isType(library))
        {
            throw new IOException("Library is not a TV Show library: " + library);
        }

        String encodedName = URLEncoder.encode(showName, "utf-8");
        String path = createLibraryEntriesPath(library.getKey()) + "&title=" + encodedName;

        Parser<MediaDirectory> parser = parserFactory.createMediaDirecoryParser();

        return parser.parse(path);

    }

    /**
     * Get the TV show seasons
     *
     * @param library
     * @param tvShowName
     * @return
     * @throws IOException
     */
    public List<MediaDirectory> getTvShowSeasonsByName(MediaDirectory library, String tvShowName) throws IOException
    {
        if (!DirectoryType.TV_SERIES.isType(library))
        {
            throw new IOException("Library is not a TV Show library: " + library);
        }

        Optional<MediaDirectory> opt = getTvShowListingByLibraryKey(library.getKey()).stream()
                .filter(e -> e.getTitle().equals(tvShowName))
                .findFirst();

        if (opt.isPresent())
        {
            return getTvShowSeasons(opt.get());
        }

        String msg = String.format("TV show '%s' not found in library '%s'", library.getTitle(), tvShowName);
        throw new IOException(msg);
    }

    /**
     * Get Seasons fora given TV Show.
     *
     * @param show TV show
     * @return List of seasons.
     * @throws IOException thrown if errors occur while reading Plex data
     */
    public List<MediaDirectory> getTvShowSeasons(MediaDirectory show) throws IOException
    {

        String key = show.getAttribute(TV_SHOW_KEY_ATTR);
        return getTvShowSeasonsByShowKey(key);
    }

    /**
     * Get seasons by a given TV show key.
     *
     * @param key show key
     * @return seasons
     * @throws IOException thrown if errors occur while reading data
     */
    public List<MediaDirectory> getTvShowSeasonsByShowKey(String key) throws IOException
    {
        String path = String.format(CHILDREN_PATH_TEMPLATE, key) + createAuthTokenPart();
        List<MediaDirectory> list = new ArrayList<>();
        Consumer<MediaDirectory> consumer = e ->
        {
            if (DirectoryType.TV_SERIES_SEASON.isType(e))
            {
                list.add(e);
            }
        };

        Parser<MediaDirectory> parser = parserFactory.createMediaDirecoryParser();
        parser.parse(path, consumer);

        return list;
    }

    public List<MediaDirectory> getTvShowListingByLibraryName(String libraryName) throws IOException
    {
        Optional<MediaDirectory> opt = getLibraries().stream()
                .filter(e -> e.getTitle().equals(libraryName))
                .findFirst();

        if (opt.isPresent())
        {
            return getTvShowListingByLibraryKey(opt.get().getKey());
        }

        throw new IOException(libraryName + " not found");
    }

    public List<MediaDirectory> getTvShowListingByLibraryKey(String key) throws IOException
    {
        Parser<MediaDirectory> parser = parserFactory.createMediaDirecoryParser();

        String path = createLibraryEntriesPath(key);

        return parser.parse(path);
    }

    public List<Video> getEpisodesByShowName(MediaDirectory library, String tvShowName) throws IOException
    {
        if (!DirectoryType.TV_SERIES.isType(library))
        {
            throw new IOException("Library parameter is not a TV Show library: " + library);
        }

        Optional<MediaDirectory> opt = getTvShowListingByLibraryKey(library.getAttribute(TV_SHOW_KEY_ATTR))
                .stream().filter(e -> e.getTitle().equals(tvShowName)).findFirst();

        if (opt.isPresent())
        {
            getEpisodesByShow(opt.get());
        }

        String msg = String.format("TV show '%s' not found in library '%s'", library.getTitle(), tvShowName);
        throw new IOException(msg);

    }

    public List<Video> getEpisodesBySeason(MediaDirectory season) throws IOException
    {
        if (!DirectoryType.TV_SERIES_SEASON.isType(season))
        {
            throw new IOException("Parmeter is not a TV Show Season: " + season);
        }

        String[] seasonKey = season.getKey().split("/");
        String id = seasonKey[seasonKey.length - 1];
        return getEpisodesBySeasonId(id);
    }

    public List<Video> getEpisodesBySeasonId(String seasonId) throws IOException
    {
        Parser<Video> parser = parserFactory.createVideoParser();
        String path = String.format(CHILDREN_PATH_TEMPLATE, seasonId) + createAuthTokenPart();
        return parser.parse(path);
    }

    public List<Video> getEpisodesByShow(MediaDirectory tvShow) throws IOException
    {
        if (!DirectoryType.TV_SERIES.isType(tvShow))
        {
            throw new IOException("Parameter is not a TV Show: " + tvShow);
        }

        return getEpisodesByShowKey(tvShow.getAttribute(TV_SHOW_KEY_ATTR));
    }

    public List<Video> getEpisodesByShowKey(String showKey) throws IOException
    {
        Parser<Video> parser = parserFactory.createVideoParser();

        String path = String.format(ALL_SERIES_EPISODES_PATH_TEMPLATE, showKey) + createAuthTokenPart();

        return parser.parse(path);
    }

    public URL getUrl(String uri) throws MalformedURLException
    {
        String queryString = uri.startsWith("/") ? uri : "/" + uri;
        if (!queryString.contains(PLEX_TOKEN_VAR))
        {
            queryString = String.format("%s?%s=%s", queryString, PLEX_TOKEN_VAR, authenticationToken);
        }

        String base = hasProtocol() ? server : "http://" + server;
        String spec = String.format("%s%s", base, queryString);

        return new URL(spec);
    }

    public boolean hasProtocol()
    {
        return server.startsWith("http://") || server.startsWith("https://");
    }

    public void download(String uri, Consumer<InputStream> consumer) throws IOException
    {
        URL url = getUrl(uri);
        try (InputStream is = url.openStream())
        {
            consumer.accept(is);
        }
    }

    public void download(String uri, Path localFile) throws IOException
    {
        URL url = getUrl(uri);
        try (InputStream is = url.openStream();
             OutputStream os = Files.newOutputStream(localFile))
        {
            while (true)
            {
                byte[] buff = new byte[1024];
                int count = is.read(buff);
                if (count < 1)
                {
                    break;
                }

                os.write(buff, 0, count);
            }
        }
    }

    public List<Video> getVideoCurrentlyPlaying() throws IOException
    {
        String uri = String.format("/status/sessions?X-Plex-Token=%s", this.authenticationToken);
        Parser<Video> parser = parserFactory.createVideoParser();

        List<Video> videos = new ArrayList<>();
        parser.parse(uri, videos::add);

        return videos;
    }

}
