/* 
 * Copyright 2019 roberto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.plex;

import com.iamcodepoet.util.EqualsBuilder;
import java.util.Objects;

/**
 *
 * @author Roberto C. Benitez
 */
public class VideoPart implements java.io.Serializable
{

    private static final long serialVersionUID = 338886577286406390L;

    private int id;
    private String key;
    private String file;
    private String audioProfile;
    private String container;
    private String videoProfile;
    private long duration;
    private long size;
    
    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public String getFile()
    {
        return file;
    }

    public void setFile(String file)
    {
        this.file = file;
    }

    public String getAudioProfile()
    {
        return audioProfile;
    }

    public void setAudioProfile(String audioProfile)
    {
        this.audioProfile = audioProfile;
    }

    public String getContainer()
    {
        return container;
    }

    public void setContainer(String container)
    {
        this.container = container;
    }

    public String getVideoProfile()
    {
        return videoProfile;
    }

    public void setVideoProfile(String videoProfile)
    {
        this.videoProfile = videoProfile;
    }

    public long getDuration()
    {
        return duration;
    }

    public void setDuration(long duration)
    {
        this.duration = duration;
    }

    public long getSize()
    {
        return size;
    }

    public void setSize(long size)
    {
        this.size = size;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(file);
    }

    @Override
    public boolean equals(Object obj)
    {
        return EqualsBuilder.withTest(VideoPart::getFile)
                .areEqual(this, obj);
    }

    @Override
    public String toString()
    {
        return file;
    }

}
