/* 
 * Copyright 2019 roberto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.plex;

import com.iamcodepoet.util.EqualsBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author Roberto C. Benitez
 */
public class MediaDirectory implements java.io.Serializable
{
    
    private static final long serialVersionUID = 4694228211438761140L;
    
    private final Map<String, String> attributes = new HashMap<>();
    
    private final List<MediaDirectory> children = new ArrayList<>();
    
    public String getKey()
    {
        return attributes.get("key");
    }
    
    public void setKey(String key)
    {
        attributes.put("key", key);
    }
    
    public String getType()
    {
        return attributes.get("type");
    }
    
    public void setType(String type)
    {
        attributes.put("type", type);
    }
    
    public String getTitle()
    {
        return attributes.get("title");
    }
    
    public void setTitle(String title)
    {
        attributes.put("title", title);
    }
    
    public String getIndex()
    {
        return attributes.get("index");
    }
    
    public String getParentIndex()
    {
        return attributes.get("parentIndex");
    }
    
    public String getAttribute(String name)
    {
        return attributes.get(name);
    }
    
    public void setAttribute(String name, String value)
    {
        attributes.put(name, value);
    }
    
    public Map<String, String> getAttributes()
    {
        return attributes;
    }
    
    public void addChild(MediaDirectory child)
    {
        children.add(child);
    }
    
    public List<MediaDirectory> getChildren()
    {
        return children;
    }
    
    @Override
    public int hashCode()
    {
        return Objects.hash(getKey(), getType(), getTitle());
    }
    
    @Override
    public boolean equals(Object obj)
    {
        return EqualsBuilder.withTest(MediaDirectory::getKey)
                .appendTest(MediaDirectory::getType)
                .appendTest(MediaDirectory::getTitle)
                .areEqual(this, obj);
    }
    
    @Override
    public String toString()
    {
        String string = getTitle();
        String type = getType();
        if (!(type == null || type.isEmpty()))
        {
            string += "(" + type + ")";
        }
        
        return string;
    }
}
