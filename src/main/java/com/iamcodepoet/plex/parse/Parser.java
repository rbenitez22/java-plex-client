/*
 * Copyright 2019 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.plex.parse;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 *
 * @author Roberto C. Benitez
 * @param <V> type of parsed value
 */
public abstract class Parser<V>
{

    public static final String LOCATIONS_KEY = "mediaLocations";
    public static final String LOCATION_TAG = "Location";
    public static final String CONTAINER_TAG = "MediaContainer";
    public static final String LIBRARY_TAG = "Directory";
    public static final String VIDEO_TAG = "Video";
    public static final String PART_TAG = "Part";

    protected final String plexServer;
    protected final String acceptDataType;

    public Parser(String plexServer)
    {
        this(plexServer, "application/xml");
    }

    public Parser(String plexServer, String acceptDataType)
    {
        this.plexServer = plexServer;
        this.acceptDataType = acceptDataType;
    }

    public List<V> parse(String path) throws IOException
    {
        List<V> libs = new ArrayList<>();
        parse(path, lib -> libs.add(lib));

        return libs;
    }

    public void parse(String path, Consumer<V> consumer) throws IOException
    {
        URL url = getUrl(path);
        URLConnection conn = url.openConnection();
        conn.setRequestProperty("Accept", acceptDataType);
        conn.connect();

        try (InputStream is = conn.getInputStream())
        {
            parse(is, consumer);
        }
    }

    public abstract void parse(final InputStream is, Consumer<V> consumer) throws IOException;

    protected URL getUrl(String path) throws MalformedURLException
    {
        String base = hasProtocol() ? plexServer : "http://" + plexServer;
        String string = path.startsWith("/") ? path : "/" + path;
        String spec = String.format("%s%s", base, string);

        return new URL(spec);
    }

    public boolean hasProtocol()
    {
        return plexServer.startsWith("http://") || plexServer.startsWith("https://");
    }

}
