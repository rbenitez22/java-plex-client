/*
 * Copyright 2019 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.plex.parse;

import com.iamcodepoet.plex.MediaDirectory;
import com.iamcodepoet.plex.Video;
import java.util.ServiceLoader;

/**
 * Abstract Parser factory to create all necessary parsers.S
 *
 * @author Roberto C. Benitez
 */
public abstract class ParserFactory
{

    protected String plexServer;

    public ParserFactory()
    {
    }

    public ParserFactory(String plexServer)
    {
        this.plexServer = plexServer;
    }

    public void setPlexServer(String plexServer)
    {
        this.plexServer = plexServer;
    }

    public abstract String getType();

    public abstract Parser<MediaDirectory> createMediaDirecoryParser();

    public abstract Parser<MediaDirectory> createMediaLibraryParser();

    public abstract Parser<Video> createVideoParser();

    public static ParserFactory createFactory(String type) throws ParserFactoryException
    {
        ServiceLoader<ParserFactory> loader = ServiceLoader.load(ParserFactory.class);
        for (ParserFactory factory : loader)
        {
            if (factory.getType().equalsIgnoreCase(type))
            {
                return factory;
            }
        }

        throw new ParserFactoryException("Could not find a ParserFactory for type " + type);
    }

}
