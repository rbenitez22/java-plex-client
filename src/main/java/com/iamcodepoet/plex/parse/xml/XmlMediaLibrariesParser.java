/* 
 * Copyright 2019 roberto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.plex.parse.xml;

import com.iamcodepoet.plex.MediaDirectory;
import com.iamcodepoet.util.XmlUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

public class XmlMediaLibrariesParser extends XmlMediaDirectoryParser
{
    public static final String LOCATIONS_KEY = "mediaLocations";
    private static final String LOCATION_TAG = "Location";

    public XmlMediaLibrariesParser(String plexServer)
    {
        super(plexServer);
    }
    
    @Override protected MediaDirectory createDirectory(XMLStreamReader reader) throws XMLStreamException
    {
        MediaDirectory lib = new MediaDirectory();

        XmlUtils.loadAttributes(reader, lib.getAttributes());
        List<String> locations = new ArrayList<>();
        
        while(!XmlUtils.isElementEnd(reader, XmlMediaDirectoryParser.LIBRARY_TAG))
        {
            reader.next();
            if(XmlUtils.isElementStart(reader, LOCATION_TAG))
            {
                XmlUtils.getAttributes(reader).entrySet()
                        .stream().filter(e->"path".equals(e.getKey()))
                        .forEach(e -> locations.add(e.getValue()));
            }
        }
        
        lib.setAttribute(LOCATIONS_KEY, locations.stream().collect(Collectors.joining(",")));

        return lib;
    }
    
    
}
