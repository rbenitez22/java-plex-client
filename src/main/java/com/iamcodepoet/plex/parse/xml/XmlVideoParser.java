/* 
 * Copyright 2019 roberto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.plex.parse.xml;

import com.iamcodepoet.plex.Video;
import com.iamcodepoet.plex.VideoPart;
import com.iamcodepoet.plex.parse.Parser;
import com.iamcodepoet.util.ValueUtils;
import com.iamcodepoet.util.XmlUtils;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 *
 * @author roberto
 */
public class XmlVideoParser extends Parser<Video>
{

    public XmlVideoParser(String plexServer)
    {
        super(plexServer);
    }

    @Override
    public void parse(final InputStream is, Consumer<Video> consumer) throws IOException
    {
        try
        {
            XMLInputFactory factory = XMLInputFactory.newFactory();
            XMLStreamReader reader = factory.createXMLStreamReader(is);
            while (reader.hasNext())
            {
                reader.next();

                if (XmlUtils.isElementStart(reader, VIDEO_TAG))
                {
                    Video video = parseVideo(reader);

                    parseVideoParts(reader, video);

                    consumer.accept(video);

                }
                else if (XmlUtils.isElementEnd(reader, CONTAINER_TAG))
                {
                    break;
                }
            }
        }
        catch (XMLStreamException | FactoryConfigurationError e)
        {
            throw new IOException(e.getMessage(), e);
        }
    }

    private Video parseVideo(XMLStreamReader reader)
    {
        Map<String, String> videoAttr = new HashMap<>();
        XmlUtils.loadAttributes(reader, videoAttr);

        Video video = new Video();
        video.setKey(videoAttr.get("key"));
        video.setType(videoAttr.get("type"));
        video.setTitle(videoAttr.get("title"));
        video.setParentTitle(videoAttr.get("parentTitle"));
        video.setGrandParentTitle(videoAttr.get("grandparentTitle"));

        String description = videoAttr.getOrDefault("summary", "");
        video.setDescription(description);

        int index = ValueUtils.getInt(videoAttr, "index");
        int parentIndex = ValueUtils.getInt(videoAttr, "parentIndex");

        video.setIndex(index);
        video.setParentIndex(parentIndex);
        LocalDate originalDate = ValueUtils.getDateFromString(videoAttr, "originallyAvailableAt");
        video.setOriginaldate(originalDate);

        LocalDateTime updatedOn = ValueUtils.getDateTimeValueFromSeconds(videoAttr, "updatedAt");
        video.setLastUpdated(updatedOn);

        return video;
    }

    private void parseVideoParts(XMLStreamReader reader, Video video) throws NumberFormatException, XMLStreamException
    {
        while (true)
        {
            reader.next();

            if (XmlUtils.isElementStart(reader, PART_TAG))
            {
                Map<String, String> partAttr = XmlUtils.getAttributes(reader);

                VideoPart part = createVideoPart(partAttr);

                video.addPart(part);
            }
            else if (XmlUtils.isElementEnd(reader, VIDEO_TAG))
            {
                break;
            }
        }
    }

    private VideoPart createVideoPart(Map<String, String> partAttr) throws NumberFormatException
    {
        VideoPart part = new VideoPart();
        int id = Integer.parseInt(partAttr.getOrDefault("id", "0"));
        long duration = Long.parseLong(partAttr.getOrDefault("duration", "0"));
        long size = Long.parseLong(partAttr.getOrDefault("size", "0"));
        part.setId(id);
        part.setDuration(duration);
        part.setSize(size);
        part.setKey(partAttr.get("key"));
        part.setFile(partAttr.get("file"));
        part.setAudioProfile(partAttr.get("audoProfile"));
        part.setVideoProfile(partAttr.get("videoProfile"));
        part.setContainer(partAttr.get("container"));
        return part;
    }

}
