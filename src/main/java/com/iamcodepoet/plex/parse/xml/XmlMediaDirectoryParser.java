/* 
 * Copyright 2019 roberto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.plex.parse.xml;

import com.iamcodepoet.plex.MediaDirectory;
import com.iamcodepoet.plex.parse.Parser;
import com.iamcodepoet.util.XmlUtils;
import java.io.IOException;
import java.io.InputStream;
import java.util.function.Consumer;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 *
 * @author Roberto C. Benitez
 */
public class XmlMediaDirectoryParser extends Parser<MediaDirectory>
{

    public XmlMediaDirectoryParser(String plexServer)
    {
        super(plexServer);
    }

    @Override
    public void parse(final InputStream is, Consumer<MediaDirectory> consumer) throws IOException
    {
        try
        {
            XMLInputFactory factory = XMLInputFactory.newFactory();
            XMLStreamReader reader = factory.createXMLStreamReader(is);
            while (reader.hasNext())
            {
                int event = reader.next();

                if (XmlUtils.isElementStart(reader, CONTAINER_TAG))
                {
                    MediaDirectory directory = createDirectory(reader);

                    consumer.accept(directory);

                }
                else if (XmlUtils.isElementEnd(reader, CONTAINER_TAG))
                {
                    break;
                }
            }
        }
        catch (XMLStreamException | FactoryConfigurationError e)
        {
            throw new IOException(e.getMessage(), e);
        }
    }

    protected MediaDirectory createDirectory(XMLStreamReader reader) throws XMLStreamException
    {
        MediaDirectory lib = new MediaDirectory();

        XmlUtils.loadAttributes(reader, lib.getAttributes());

        return lib;
    }

}
