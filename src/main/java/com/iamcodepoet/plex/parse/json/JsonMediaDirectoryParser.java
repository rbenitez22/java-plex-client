/* 
 * Copyright 2019 roberto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.plex.parse.json;

import com.iamcodepoet.plex.MediaDirectory;
import com.iamcodepoet.plex.parse.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.util.function.Consumer;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonString;
import javax.json.JsonValue;

/**
 *
 * @author Roberto C. Benitez
 */
public class JsonMediaDirectoryParser extends Parser<MediaDirectory>
{

    public JsonMediaDirectoryParser(String plexServer)
    {
        super(plexServer, "application/json");
    }

    @Override
    public void parse(final InputStream is, Consumer<MediaDirectory> consumer) throws IOException
    {
        try (JsonReader reader = Json.createReader(is))
        {

            JsonObject root = reader.readObject();
            JsonObject container = root.getJsonObject(CONTAINER_TAG);

            getContainerDataArray(container).stream().map(e -> e.asJsonObject())
                    .map(this::createDirectory)
                    .forEach(consumer);
        }
        catch (Exception e)
        {
            throw new IOException(e.getMessage(), e);
        }
    }

    protected JsonArray getContainerDataArray(JsonObject container)
    {
        return container.getJsonArray("Metadata");
    }

    protected MediaDirectory createDirectory(JsonObject object)
    {
        MediaDirectory lib = new MediaDirectory();

        object.entrySet()
                .stream()
                .filter(e -> e.getValue().getValueType() != JsonValue.ValueType.OBJECT)
                .forEach(e -> lib.setAttribute(e.getKey(), getJsonValueString(e.getValue())));

        return lib;
    }

    protected String getJsonValueString(JsonValue value)
    {
        if (value == null || value.getValueType() == JsonValue.ValueType.NULL)
        {
            return null;
        }

        switch (value.getValueType())
        {
            case STRING:
                JsonString string = (JsonString) value;
                return string.getString();
            case NUMBER:
                JsonNumber number = (JsonNumber) value;
                return number.toString();
            default:
                return value.toString();

        }

    }

}
