/* 
 * Copyright 2019 roberto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.plex.parse.json;

import com.iamcodepoet.plex.Video;
import com.iamcodepoet.plex.VideoPart;
import com.iamcodepoet.plex.parse.Parser;
import com.iamcodepoet.util.ValueUtils;
import com.iamcodepoet.util.XmlUtils;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 *
 * @author roberto
 */
public class JsonVideoParser extends Parser<Video>
{

    public JsonVideoParser(String plexServer)
    {
        super(plexServer, "application/json");
    }

    @Override
    public void parse(final InputStream is, Consumer<Video> consumer) throws IOException
    {
        try (JsonReader reader = Json.createReader(is))
        {
            JsonObject root = reader.readObject();
            JsonObject container = root.getJsonObject(CONTAINER_TAG);
            container.getJsonArray("Metadata").stream()
                    .map(jsonValueToVideoMapper())
                    .forEach(consumer);
        }
        catch (Exception e)
        {
            throw new IOException(e.getMessage(), e);
        }
    }

    private Function<JsonValue, Video> jsonValueToVideoMapper()
    {
        return value ->
        {
            JsonObject json = value.asJsonObject();

            Video video = new Video();
            video.setKey(json.getString("key"));
            video.setType(json.getString("type"));
            video.setTitle(json.getString("title"));
            video.setParentTitle(json.getString("parentTitle", ""));
            video.setGrandParentTitle(json.getString("grandparentTitle", ""));

            String description = json.getString("summary", "");
            video.setDescription(description);

            int index = json.getInt("index", 0);
            int parentIndex = json.getInt("parentIndex", 0);

            video.setIndex(index);
            video.setParentIndex(parentIndex);

            video.setParentIndex(parentIndex);
            LocalDate originalDate = ValueUtils.getDateFromJsonString(json, "originallyAvailableAt");
            video.setOriginaldate(originalDate);

            int seconds = json.getInt("updatedAt", 0);
            LocalDateTime updatedOn = LocalDateTime.ofEpochSecond(seconds, 0, ZoneOffset.UTC);

            video.setLastUpdated(updatedOn);

            if (json.containsKey("Media"))
            {
                JsonArray media = json.getJsonArray("Media");

                media.stream().map(e -> e.asJsonObject()).filter(e -> e.containsKey("Part"))
                        .map(jsonValueToVideoPartMapper())
                        .forEach(video::addPart);
            }

            return video;

        };
    }

    private Function<JsonValue, VideoPart> jsonValueToVideoPartMapper()
    {
        return value ->
        {
            JsonObject json = value.asJsonObject();

            VideoPart part = new VideoPart();
            int id = json.getInt("id", 0);
            long duration = getLong(json, "duration");
            long size = getLong(json, "size");

            part.setId(id);
            part.setDuration(duration);
            part.setSize(size);
            part.setKey(json.getString("key", ""));
            part.setFile(json.getString("file", ""));
            part.setAudioProfile(json.getString("audoProfile", ""));
            part.setVideoProfile(json.getString("videoProfile", ""));
            part.setContainer(json.getString("container", ""));

            return part;

        };
    }

    private long getLong(JsonObject json, String key)
    {
        if (json.containsKey(key))
        {
            JsonValue value = json.get(key);
            if (value.getValueType() == JsonValue.ValueType.NUMBER)
            {
                return ((JsonNumber) value).longValue();
            }
        }

        return 0;
    }

    private void parseVideoParts(XMLStreamReader reader, Video video) throws NumberFormatException, XMLStreamException
    {
        while (true)
        {
            reader.next();

            if (XmlUtils.isElementStart(reader, PART_TAG))
            {
                Map<String, String> partAttr = XmlUtils.getAttributes(reader);

                VideoPart part = createVideoPart(partAttr);

                video.addPart(part);
            }
            else if (XmlUtils.isElementEnd(reader, VIDEO_TAG))
            {
                break;
            }
        }
    }

    private VideoPart createVideoPart(Map<String, String> partAttr) throws NumberFormatException
    {
        VideoPart part = new VideoPart();
        int id = Integer.parseInt(partAttr.getOrDefault("id", "0"));
        long duration = Long.parseLong(partAttr.getOrDefault("duration", "0"));
        long size = Long.parseLong(partAttr.getOrDefault("size", "0"));
        part.setId(id);
        part.setDuration(duration);
        part.setSize(size);
        part.setKey(partAttr.get("key"));
        part.setFile(partAttr.get("file"));
        part.setAudioProfile(partAttr.get("audoProfile"));
        part.setVideoProfile(partAttr.get("videoProfile"));
        part.setContainer(partAttr.get("container"));
        return part;
    }

}
