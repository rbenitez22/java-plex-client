/*
 * Copyright 2019 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.plex.parse.json;

import com.iamcodepoet.plex.MediaDirectory;
import com.iamcodepoet.plex.Video;
import com.iamcodepoet.plex.parse.Parser;
import com.iamcodepoet.plex.parse.ParserFactory;

public class JsonParserFactory extends ParserFactory
{

    public JsonParserFactory()
    {
    }

    public JsonParserFactory(String plexServer)
    {
        super(plexServer);
    }

    @Override
    public String getType()
    {
        return "JSON";
    }

    @Override
    public Parser<MediaDirectory> createMediaDirecoryParser()
    {
        return new JsonMediaDirectoryParser(plexServer);
    }

    @Override
    public Parser<MediaDirectory> createMediaLibraryParser()
    {
        return new JsonMediaLibrariesParser(plexServer);
    }

    @Override
    public Parser<Video> createVideoParser()
    {
        return new JsonVideoParser(plexServer);
    }

}
