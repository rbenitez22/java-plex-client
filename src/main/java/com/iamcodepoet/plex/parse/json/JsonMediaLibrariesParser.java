/* 
 * Copyright 2019 roberto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.plex.parse.json;

import javax.json.JsonArray;
import javax.json.JsonObject;

public class JsonMediaLibrariesParser extends JsonMediaDirectoryParser
{

    public JsonMediaLibrariesParser(String plexServer)
    {
        super(plexServer);
    }

    @Override
    protected JsonArray getContainerDataArray(JsonObject container)
    {
        return container.getJsonArray("Directory");
    }

}
