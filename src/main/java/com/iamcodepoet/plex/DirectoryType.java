/* 
 * Copyright 2019 roberto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.plex;

/**
 *
 * @author roberto
 */
public enum DirectoryType
{
    MOVIE("movie"),MUSIC("artist"),PHOTO("photo"),TV_SERIES("show"),TV_SERIES_SEASON("season");
   
    final String plexTypeName;

    private DirectoryType(String plexTypeName)
    {
        this.plexTypeName = plexTypeName;
    }

    public String getPlexTypeName()
    {
        return plexTypeName;
    }

    public boolean isType(MediaDirectory directory)
    {
        return directory != null && isType(directory.getType());
    }
    
    public boolean isType(String input)
    {
        return plexTypeName.equals(input);
    }
    
}
