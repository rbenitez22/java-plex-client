/* 
 * Copyright 2019 roberto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.plex;

import com.iamcodepoet.util.EqualsBuilder;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Roberto C. Benitez
 */
public class Video implements java.io.Serializable
{

    private static final long serialVersionUID = 6643590959085987876L;

    private String key;
    private String type;
    private String title;
    private String description;

    private String parentTitle;
    private String grandParentTitle;
    private int index;
    private int parentIndex;
    private LocalDate originaldate;
    private LocalDateTime lastUpdated;

    private final List<VideoPart> parts = new ArrayList<>();

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getParentTitle()
    {
        return parentTitle;
    }

    public void setParentTitle(String parentTitle)
    {
        this.parentTitle = parentTitle;
    }

    public String getGrandParentTitle()
    {
        return grandParentTitle;
    }

    public void setGrandParentTitle(String grandParentTitle)
    {
        this.grandParentTitle = grandParentTitle;
    }

    public int getIndex()
    {
        return index;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }

    public int getParentIndex()
    {
        return parentIndex;
    }

    public void setParentIndex(int parentIndex)
    {
        this.parentIndex = parentIndex;
    }

    public LocalDate getOriginaldate()
    {
        return originaldate;
    }

    public void setOriginaldate(LocalDate originaldate)
    {
        this.originaldate = originaldate;
    }

    public LocalDateTime getLastUpdated()
    {
        return lastUpdated;
    }

    public void setLastUpdated(LocalDateTime lastUpdated)
    {
        this.lastUpdated = lastUpdated;
    }

    public List<VideoPart> getParts()
    {
        return parts;
    }

    public void addPart(VideoPart part)
    {
        parts.add(part);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(key);
    }

    @Override
    public boolean equals(Object obj)
    {
        return EqualsBuilder.withTest(Video::getKey)
                .areEqual(this, obj);
    }

    @Override
    public String toString()
    {
        return title;
    }
}
