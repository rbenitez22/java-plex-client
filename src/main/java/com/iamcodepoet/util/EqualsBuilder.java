/*
 * Copyright (C) 2018 Roberto C. Benitez
 * 
 */

package com.iamcodepoet.util;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

/**
 *
 * @author Roberto C. Benitez
 * @param <T> Object Type Parameter
 */
public class EqualsBuilder<T>
{

    private final List<Function<T, Object>> tests = new LinkedList<>();

    public EqualsBuilder<T> appendTest(Function<T, Object> test)
    {
        tests.add(test);
        return this;
    }

    public boolean areEqual(T object, Object otherObject)
    {
        if (object == otherObject)
        {
            return true;
        }

        if (object == null || otherObject == null)
        {
            return false;
        }

        if (object.getClass() != otherObject.getClass())
        {
            return false;
        }

        final T other = (T) otherObject;

        for (Function<T, Object> function : tests)
        {
            Object value1 = function.apply(object);
            Object value2 = function.apply(other);
            if (!Objects.equals(value1, value2))
            {
                return false;
            }
        }

        return true;
    }

    public static <T> EqualsBuilder<T> withTest(Function<T, Object> test)
    {
        EqualsBuilder<T> builder = new EqualsBuilder<>();
        builder.appendTest(test);

        return builder;
    }
}
