/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iamcodepoet.util;

import java.util.HashMap;
import java.util.Map;
import javax.xml.stream.XMLStreamReader;

/**
 *
 * @author roberto
 */
public final class XmlUtils
{

    private XmlUtils()
    {
    }

    public static Map<String, String> getAttributes(XMLStreamReader reader)
    {
        Map<String, String> map = new HashMap<>();

        loadAttributes(reader, map);

        return map;
    }

    public static void loadAttributes(XMLStreamReader reader, Map<String, String> attributes)
    {
        for (int i = 0; i < reader.getAttributeCount(); i++)
        {
            String name = reader.getAttributeLocalName(i);
            String value = reader.getAttributeValue(i);
            attributes.put(name, value);
        }
    }

    public static boolean isElementStart(XMLStreamReader reader, String element)
    {
        return reader.getEventType() == XMLStreamReader.START_ELEMENT
                && reader.getLocalName().equals(element);
    }

    public static boolean isElementEnd(XMLStreamReader reader, String element)
    {
        return reader.getEventType() == XMLStreamReader.END_ELEMENT
                && reader.getLocalName().equals(element);
    }
}
